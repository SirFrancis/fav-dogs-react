import styled from 'styled-components';
import React from 'react';
import {HeaderProps} from './Header.types';

const Title = styled.h1`
  text-align: center;
  flex: 1;
`;
const StyledHeader = styled.header`
  display: flex;
  flex-direction: row;
  @media screen and (max-width: 479px) {
    flex-direction: column;
  }
  justify-content: space-between;
  align-items: center;
  padding: 10px;
`;

export function Header({title, leftElement, rightElement}: HeaderProps) {
  return (
    <StyledHeader>
      {leftElement}
      <Title>{title}</Title>
      {rightElement}
    </StyledHeader>
  );
}
