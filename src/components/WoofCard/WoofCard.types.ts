import {Woof} from '../../../schema';

export interface WoofCardProps {
  onSelected: (woof: Woof) => void;
  woof: Partial<Woof>;
}
