import React from 'react';

export interface ButtonProps extends React.PropsWithChildren {
  onClick: () => void;
}
