import React, {useEffect} from 'react';
import {FavouritesListContextType} from './FavouritesListProvider.types';
import {Woof} from '../../../schema';
import {createContext} from '../../utils/createContext';

export const [FavouritesContext, useFavouritesContext] = createContext<FavouritesListContextType>();

export const FavouritesListProvider = ({children}: React.PropsWithChildren<{}>) => {
  const [favouritesList, setFavouritesList] = React.useState<Woof[]>([]);

  const addFavourite = (favourite: Woof) => {
    setFavouritesList([...favouritesList, favourite]);
  };
  const removeFavourite = (id: string) => {
    setFavouritesList(favouritesList.filter(favourite => favourite.id !== id));
  };
  const isFavourite = (id: string) => {
    return favouritesList.find(favourite => favourite.id !== id) !== undefined;
  };

  useEffect(() => {
    if (favouritesList) {
      localStorage.setItem('favouritesList', JSON.stringify(favouritesList));
    }
  }, [favouritesList]);

  return (
    <FavouritesContext value={{favourites: favouritesList, isFavourite, addFavourite, removeFavourite}}>
      {children}
    </FavouritesContext>
  );
};
