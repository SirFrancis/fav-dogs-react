import {Woof} from '../../../schema';

export interface FavouritesListContextType {
  favourites: Woof[];
  addFavourite: (favourite: Woof) => void;
  removeFavourite: (id: string) => void;
  isFavourite: (id: string) => boolean;
}
