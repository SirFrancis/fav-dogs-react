import React from 'react';
import ReactDOM from 'react-dom/client';
import Main from './pages/Main';
import './base.css';

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement);

root.render(<Main />);
