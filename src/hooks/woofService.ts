import axios from 'axios';
import {useCallback} from 'react';
import {Woof} from '../../schema';
import {useFavouritesContext} from '../providers/FavouritesList/FavouritesListProvider';

const BASE_URL = 'https://random.dog/';
interface WoofService {
  getWoof: () => Promise<Woof>;
  getWoofs: (count: number) => Promise<Woof[]>;
}

export function useWoofService(): WoofService {
  const favouritesContext = useFavouritesContext();
  const getWoof = useCallback(async () => {
    const {data} = await axios.get(`${BASE_URL}woof.json`);
    return {
      url: data.url,
      id: data.url.replace(BASE_URL, '')
    };
  }, []);
  const getWoofs = useCallback(async (count: number) => {
    const woofs: Woof[] = [];
    while (woofs.length < count) {
      const woof = await getWoof();
      if (
        !favouritesContext.isFavourite(woof.id) &&
        !woof.id.endsWith('.mp4') &&
        !woof.id.endsWith('.webm') &&
        !woofs.find(w => w.id === woof.id)
      ) {
        woofs.push(woof);
      }
    }
    return woofs;
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return {
    getWoof,
    getWoofs
  };
}
