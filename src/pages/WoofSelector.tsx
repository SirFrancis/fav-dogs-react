import {WoofCard} from '../components/WoofCard/WoofCard';
import styled from 'styled-components';
import {useCallback, useEffect, useState} from 'react';
import {Woof} from '../../schema';
import {Button} from '../components/Button/Button';
import {useWoofService} from '../hooks/woofService';
import {useFavouritesContext} from '../providers/FavouritesList/FavouritesListProvider';
import {useNavigate} from 'react-router-dom';
import {Header} from '../components/Header/Header';

const WoofList = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  flex-grow: 1;
`;
const WoofSelectorDiv = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`;
const NUMBER_OF_WOOFS = 6;
export function WoofSelector() {
  const [woofsList, setWoofsList] = useState<Partial<Woof>[]>(Array.from({length: 6}, () => ({})));
  const {getWoofs} = useWoofService();
  const navigate = useNavigate();
  const {addFavourite} = useFavouritesContext();

  const reloadWoofs = useCallback(() => {
    // set woofs list to 6 empty woofs
    setWoofsList(Array.from({length: 6}, () => ({})));
    getWoofs(NUMBER_OF_WOOFS).then(woofs => {
      setWoofsList(woofs);
    });
  }, [getWoofs, setWoofsList]);
  useEffect(() => {
    if (reloadWoofs) {
      reloadWoofs();
    }
  }, [reloadWoofs]);
  return (
    <WoofSelectorDiv>
      <Header
        title="Select your favourite woof!"
        leftElement={<Button onClick={() => navigate('/favourites')}>See your favourites</Button>}
        rightElement={<Button onClick={() => reloadWoofs()}>Reload woofs</Button>}
      />
      <WoofList>
        {woofsList.map((woof, i) => (
          <WoofCard
            woof={woof}
            key={i}
            onSelected={(woof: Woof) => {
              addFavourite(woof);
              reloadWoofs();
            }}
          />
        ))}
      </WoofList>
    </WoofSelectorDiv>
  );
}
