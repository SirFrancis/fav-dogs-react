import React from 'react';
import {createBrowserRouter, RouterProvider} from 'react-router-dom';
import {WoofSelector} from './WoofSelector';
import {Favourites} from './Favourites';
import {FavouritesListProvider} from '../providers/FavouritesList/FavouritesListProvider';

const router = createBrowserRouter([
  {
    path: '/',
    element: <WoofSelector />
  },
  {
    path: '/favourites',
    element: <Favourites />
  }
]);

function Main() {
  return (
    <FavouritesListProvider>
      <RouterProvider router={router} />
    </FavouritesListProvider>
  );
}

export default Main;
