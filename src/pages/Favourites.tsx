import {Button} from '../components/Button/Button';
import {Header} from '../components/Header/Header';
import {useNavigate} from 'react-router-dom';
import styled from 'styled-components';
import {WoofCard} from '../components/WoofCard/WoofCard';
import {Woof} from '../../schema';
import {useFavouritesContext} from '../providers/FavouritesList/FavouritesListProvider';
const FavouritesDiv = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`;
const WoofList = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  flex-grow: 1;
`;
export function Favourites() {
  const navigate = useNavigate();
  const {favourites, removeFavourite} = useFavouritesContext();
  return (
    <FavouritesDiv>
      <Header
        title="Your favourite woofs"
        leftElement={<Button onClick={() => navigate('/')}>Discover other woofs!</Button>}
      />
      <WoofList>
        {favourites.map((woof, i) => (
          <WoofCard
            woof={woof}
            key={i}
            onSelected={(woof: Woof) => {
              removeFavourite(woof.id);
            }}
          />
        ))}
      </WoofList>
    </FavouritesDiv>
  );
}
